# Bifurca Repositorio

Bifurca (haz un fork) de este repositorio. El repositorio resultante debe quedar como un repositorio de tu usuario. Una vez que tengas ese nuevo repositorio, utilizando la interfaz web de GitLab, modifica este fichero README.md para añadir, después de este enunciado, el texto "Este es el repositorio de `<nombre>`", donde `<nombre>` es tu nombre. Asegúrate de que el repositorio es público (visible para todos los visitantes) o interno (visible para los usuarios que se hayan autenticado).

Este es el repositorio de Sergio Sancho Martín